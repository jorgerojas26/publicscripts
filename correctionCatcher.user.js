// ==UserScript==
// @name         Correction catcher
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  try to take over the world!
// @author       Jorge Rojas - jorgeluisrojasb@gmail.com
// @match        https://render.figure-eight.io/assignments/*
// @updateURL    https://gitlab.com/jorgerojas26/publicscripts/-/raw/master/correctionCatcher.user.js
// @grant        none
// ==/UserScript==

(function() {
    if (document.title.toLowerCase().includes("correction")) {
        let w = window.open("", "_blank", "height=500", "width=500", "left=100", "top=100", "resizable=yes", "scrollbars=yes", "toolbar=yes", "menubar=no", "location=no", "directories=no", "status=yes");
        w.document.write(document.body.innerHTML);
    }
})();
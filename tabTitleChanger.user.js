// ==UserScript==
// @name         Tab Title Changer
// @namespace    http://tampermonkey.net/
// @version      1.1
// @description  try to take over the world!
// @author       Jorge Rojas - jorgeluisrojasb@gmail.com
// @updateURL    https://gitlab.com/jorgerojas26/publicscripts/-/raw/master/tabTitleChanger.user.js
// @match        https://render.figure-eight.io/assignments/*
// ==/UserScript==

(function () {
    'use strict';

    var rel = document.createElement("link");
    rel.rel = "icon";
    rel.href = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA4ElEQVQ4T2NUmPPuPwMFgJHqBjAyMDDgchI2OQwXVJpxMvCyMjJUHf2G4rFEbXYGG2lWhuRdX1DEMQzApdBNnpUh35CDwXvD51EDCIVBghY7g50MK0MSWmi7y7My5BhwMPhuJBCIrvKsDCXGnAzu6z6hhHaKDjuDmQQLQ9qer/hjgYOFgWFfCD/DvCs/GBZe+8nw+x8Dg64IM8NsFx6G+uPfGHY+/I3fAJCsqgAzQ5MVF4O+KDPDv/8MDB9//meYefkHw6JrPzFyDd68wMzIwMDCxMDw8y/u3Eb9zERqzgYA9fd3ofrh3lwAAAAASUVORK5CYII="
    document.head.append(rel);
    document.title = document.querySelector(".job-title").innerHTML;

})();